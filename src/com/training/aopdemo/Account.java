package com.training.aopdemo;

import com.training.aopdemo.dao.AccountDAO;

public class Account {
	

	private String name;
	private String serviceName;
	
	public Account(String name, String serviceName) {
		this.name = name;
		this.serviceName = serviceName;
	}

	public Account() {

	}
	
}
