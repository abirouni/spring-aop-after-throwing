package com.training.aopdemo;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.training.aopdemo.dao.AccountDAO;


public class MainDemoApp {

	public static void main(String[] args) {

		//read spring config java class
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
		
		AccountDAO theAccountDAO = context.getBean("accountDAO", AccountDAO.class);
		
		List<Account> Accounts = theAccountDAO.findAccount();
		
		System.out.println(Accounts);
		
		//close the context
		context.close(); 
	
	}

}
