package com.training.aopdemo.aspect;

import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.training.aopdemo.Account;
import com.training.aopdemo.dao.AccountDAO;

@Aspect
@Component
public class MyDemoLoggingAspecct {

	@Before("com.training.aopdemo.aspect.SharedAOPUtulities.forAllPackageNoGetterSetter()")
	public void beforeFindAccountsAdvice() {
		System.out.println("==============> @before Find Accounts Advice");
	}

	@AfterReturning(pointcut = "com.training.aopdemo.aspect.SharedAOPUtulities.forAllPackageNoGetterSetter()", returning = "result")
	public void afterReturningFindAccountsAdvice(JoinPoint theJoinPoint, List<Account> result) {
		System.out.println("==============> after Returning Find Accounts Advice" + result);

		// display the meth argument
		MethodSignature methodSig = (MethodSignature) theJoinPoint.getSignature();
		System.out.println("Method : " + methodSig);

		// display meth arguments
		// get args
		Object[] args = theJoinPoint.getArgs();
		for (Object arg : args) {
			System.out.println(arg);
		}

	}
}
