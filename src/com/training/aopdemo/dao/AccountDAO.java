package com.training.aopdemo.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.training.aopdemo.Account;

@Component
public class AccountDAO {

	public void addAccount() {
		System.out.println(getClass() + " \n : ADDING MY ACCOUNT");

	}

	public void addAccount(AccountDAO acountDao, boolean bol) {
		System.out.println(getClass() + " \n : ADDING MY ACCOUNT");

	}

	public List<Account> findAccount() {

		ArrayList<Account> myAccounts = new ArrayList<Account>();
		// create some accounts and add them
		System.out.println(" \n Inside  " + getClass());

		Account temp1 = new Account("John", "Silver");
		Account temp2 = new Account("Lucas", "Gold");
		Account temp3 = new Account("Mathiew", "Platinum");

		myAccounts.add(temp1);
		myAccounts.add(temp2);
		myAccounts.add(temp3);
		return myAccounts;
		

	}

}
